//download characters
const url = 'https://rickandmortyapi.com/api/character';
fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        return rawData.map(character => {
          //all needed data is listed below as an entity 
           let created = character.created;
           let species = character.species ;
           let img = character.image;
           let episodes = character.episode;
           let name = character.name;
           let location = character.location;
            //create element
            render (){
                let htmlcatalog= '';
                for (let i=0; i<10; i++){
                rawData.array(({created, species, img, episodes, name, location}) => {
                   htmlcatalog+=
                   <li class="character-element"> 
                       <span class="character-element-name">${name}</span>
                       <img class="character-element-img" src="${img}"/>
                       <span class="character-element-created">${created}</span>
                       <span class="character-element-species">${species}</span>
                       <span class="character-element-episodes">${episodes}</span>
                       <span class="character-element-location">${location}</span>
                    </li>
                    } 

                    const html = <ul class="character-container">
                        ${htmlcatalog}
                    </ul>;


                   const CHARACTERS=document.getElementById('characters');
                   CHARACTERS.innerHTML=html;   

                };

            }
            ////////////////////////////////////////////////////////////////////
           /*var out = '';
           for (var key in rowData){
              out+='Name: ' +rowData[key].name + '<br>';
              out+='<img src="' +rowData[key].img + '">';
               out+='Created: ' +rowData[key].created + '<br>';
               out+='Species: ' +rowData[key].species + '<br>';
             out+='Episodes: ' +rowData[key].episodes + '<br>';
              out+='Location: ' +rowData[key].location + '<br>';
              out+='<hr>'
          }*/
         //////////////////////////////////////////////////////////////////////  
          /*  document.querySelector('[class^="card"]').innerHTML='<ul class="list"></ul>'
            for (key in rowData){
           let row.innerHTML='<li>${key}</li>'
           document.querySelector('.character').appendChild(row)
           for (let i=0; i<rowData[key].length; i++){
               let row=document.createElement('li')
           }
           document.createElement('li')
           row.innerHTML
        //const item=document.createElement('li');
            //append element*/
        });
    
    .catch((error) => {
        console.log(JSON.stringify(error));}
    }); 
    
    document.querySelector('#sort_by_date_asc').onclick=function(){
        mySort('character-element-created');
    }
    
    document.querySelector('#sort_by_date_desc').onclick=function(){
        mySortDesc('character-element-created');
    }

    document.querySelector('#sort_by_episodes').onclick=function(){
        mySortDesc('character-element-episodes','character-element-created');
    }

    function mySort(sortType){

        let li=document.querySelector('.character-element');
        for (let i=0;i<li.children.length;i++){
            for (let j=0;j<li.children.length;j++) {
                if(+li.children[i].getAttribute(sortType)> +li.children[j].getAttribute(sortType)){
                    replaceNode=li.replaceChild(li.children[j],li.children[i]);
                    insertAfter(replaceNode,li.children[i]);
                }
            }
        }
    }

    function mySortDesc(sortType){
        let li=document.querySelector('.character-element');
        for (let i=0;i<li.children.length;i++){
            for (let j=0;j<li.children.length;j++) {
                if(+li.children[i].getAttribute(sortType)< +li.children[j].getAttribute(sortType)){
                    replaceNode=li.replaceChild(li.children[j],li.children[i]);
                    insertAfter(replaceNode,li.children[i]);
                }
            }
        }

        
    }

    function mySortDescEp(sortType){
        let li=document.querySelector('.character-element');
        for (let i=0;i<li.children.length;i++){
            for (let j=0;j<li.children.length;j++) {
                if(+li.children[i].getAttribute(sortType)< +li.children[j].getAttribute(sortType)){
                    replaceNode=li.replaceChild(li.children[j],li.children[i]);
                    insertAfter(replaceNode,li.children[i]);
                }else if (+li.children[i].getAttribute(sortType) == +li.children[j].getAttribute(sortType)){
                    let li=document.querySelector('.character-element');
                    for (let i=0;i<li.children.length;i++){
                    for (let j=0;j<li.children.length;j++) {
                    if(+li.children[i].getAttribute(sortType)< +li.children[j].getAttribute(sortType)){
                    replaceNode=li.replaceChild(li.children[j],li.children[i]);
                    insertAfter(replaceNode,li.children[i]);
                }
                }
                }

                }
            }
        }

        
    }


    window.addEventListener('scroll', function(e){
        var target = e.target;
        if(target.scrollHeight - target.scrollTop < 200) {
        loadData(++page, 10).then(function(response){
        target.appendChild(prepareHtml(response));
        })
        }
    });